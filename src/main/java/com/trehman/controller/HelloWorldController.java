package com.trehman.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for all HelloWorld mappings
 * @author trehman
 *
 */
@Controller
public class HelloWorldController {
	
	/**
	 * Adds a message to the model that should be displayed in the view. 
	 * 
	 * @param model - used to add attributes 
	 * @return view name
	 */
	@RequestMapping("/welcome")
	public String helloWorld(Model model) {
		model.addAttribute("message", "This message was passed from the Controller to the View.");
		return "welcome";
	}

}
